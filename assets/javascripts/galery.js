$(document).ready(function() {
    $('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:417,
      vThumbWidth:100,
      thumbItem:8,
      thumbMargin:0,
      slideMargin:0
    });

    //mobile
    $('#imageGallery').lightSlider({
       adaptiveHeight:false,
        item:1,
        slideMargin:0,
        loop:true,
        verticalHeight:300,
        
    });    
  });

