//start of javascript
$( document ).ready(function() {
   //function toogle menu
	 $("#nav-button").click(function(){
  		$(this).toggleClass("open");
	});
   //function toogle menu-mobile
   $("#nav-button-mobile").click(function(){
      $(this).toggleClass("open");
  });

	// function on and off sequence when menu clicked

   var buttonOn=false;
   var $push_body = $("#body-nav"),
       $content_menu = $(".wraper-content-menu ul li");

      

    var onSequence = [
        { e: $push_body,  p: {translateX: ["116.7%",0]},  o: { duration: 200,  sequenceQueue: false }},
        { e: $content_menu, p: {translateY: [0,"35px"],opacity: [1, 0]}, o: { duration: 350,  delay: 100, stagger: 350, sequenceQueue: false }}

    ];

    var offSequence = [
    	{ e: $content_menu, p: {translateY: "35px",opacity: 0}, o: { duration: 350, stagger: 350 }},
        { e: $push_body, p: {translateX: "-116.7%"}, o: { duration: 350, delay: 200, sequenceQueue: false }},
        


    ];
    // on off button
    $(".navicon-button").click(
      function() {
        buttonOn = !buttonOn;
        if (buttonOn) {
         $.Velocity.RunSequence(onSequence);
        } else {$.Velocity.RunSequence(offSequence);
        }
      });

  // function mobile menu

  

  var mobileOn=false;
  var $mobile_show = $("#nav-mobile")
      

  var onSequenceMobile = [
        { e: $mobile_show,  p: {translateY: [0,0]},  o: { duration: 200,  sequenceQueue: false }}
        

    ];

    var offSequenceMobile = [
      
        { e: $mobile_show, p: {translateY: "-200%"}, o: { duration: 350,  sequenceQueue: false }}
        


    ];
    // on off button
    $(".navicon-button").click(
      function() {
        mobileOn = !mobileOn;
        if (mobileOn) {
         $.Velocity.RunSequence(onSequenceMobile);
        } else {$.Velocity.RunSequence(offSequenceMobile);
        }
      });

    //wow js
    new WOW().init();
    //nicescroll
     $(".scrollable").niceScroll({cursorborder:"",cursorcolor:"#252420"});


});

